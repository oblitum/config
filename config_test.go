package config

import (
	"testing"
)

type Dummy struct {
	first, second string
}

func (dummy *Dummy) Configure(appliers ...Applier) (rollback Applier, err error) {
	return Configure(dummy, appliers...)
}

var defaultDummy Dummy = Dummy{first: "first", second: "second"}
var order string

func NewDummy(appliers ...Applier) (*Dummy, error) {
	dummy := defaultDummy
	if _, err := dummy.Configure(appliers...); err != nil {
		return nil, err
	}
	return &dummy, nil
}

func FirstConfiguration(first string) Applier {
	return func(configurable interface{}) (Applier, error) {
		order += first
		dummy := configurable.(*Dummy)
		previous := dummy.first
		dummy.first = first
		return FirstConfiguration(previous), nil
	}
}

func SecondConfiguration(second string) Applier {
	return func(configurable interface{}) (Applier, error) {
		order += second
		dummy := configurable.(*Dummy)
		previous := dummy.second
		dummy.second = second
		return SecondConfiguration(previous), nil
	}
}

func FailingConfiguration() Applier {
	return func(configurable interface{}) (Applier, error) {
		return nil, FailingConfigurationError{}
	}
}

type FailingConfigurationError struct{}

func (e FailingConfigurationError) Error() string {
	return "Error: Configuration 'FailingConfiguration' failed"
}

func TestConfigureWithNoArgs(t *testing.T) {
	order = ""
	dummy, err := NewDummy()

	if err != nil {
		t.Fatal("Expected no error, got", err)
	}

	if order != "" {
		t.Error("Expected empty order, got", order)
	}

	if dummy.first != "first" || dummy.second != "second" {
		t.Errorf(
			"Expected {%v, %v}, got {%v, %v}",
			"first", "second",
			dummy.first, dummy.second,
		)
	}
}

func TestConfigureWithArgs(t *testing.T) {
	order = ""
	dummy, err := NewDummy(
		FirstConfiguration("Pearl"),
		SecondConfiguration("Jam"),
	)

	if err != nil {
		t.Fatal("Expected no error, got", err)
	}

	if order != "PearlJam" {
		t.Error("Expected order 'PearlJam', got", order)
	}

	if dummy.first != "Pearl" || dummy.second != "Jam" {
		t.Errorf(
			"Expected {%v, %v}, got {%v, %v}",
			"first", "second",
			dummy.first, dummy.second,
		)
	}
}

func TestFullRollback(t *testing.T) {
	dummy, err := NewDummy()

	if err != nil {
		t.Fatal("Expected no error, got", err)
	}

	rollback, err := dummy.Configure(
		FirstConfiguration("Pearl"),
		SecondConfiguration("Jam"),
	)

	if err != nil {
		t.Fatal("Expected no error, got", err)
	}

	order = ""
	if _, err = dummy.Configure(rollback); err != nil {
		t.Fatal("Expected no error, got", err)
	}

	if order != "secondfirst" {
		t.Error("Expected order 'secondfirst', got", order)
	}

	if dummy.first != "first" || dummy.second != "second" {
		t.Errorf(
			"Expected {%v, %v}, got {%v, %v}",
			"first", "second",
			dummy.first, dummy.second,
		)
	}
}

func TestPartialRollback(t *testing.T) {
	dummy, err := NewDummy()

	if err != nil {
		t.Fatal("Expected no error, got", err)
	}

	rollback, err := dummy.Configure(
		FirstConfiguration("Pearl"),
		FailingConfiguration(),
		SecondConfiguration("Jam"),
	)

	if err == nil {
		t.Fatalf("Expected error \"%v\", got nil", FailingConfigurationError{})
	}

	if dummy.first != "Pearl" || dummy.second != "second" {
		t.Fatalf(
			"Expected {%v, %v}, got {%v, %v}",
			"first", "second",
			dummy.first, dummy.second,
		)
	}

	if _, err = dummy.Configure(rollback); err != nil {
		t.Fatal("Expected no error, got", err)
	}

	if dummy.first != "first" || dummy.second != "second" {
		t.Errorf(
			"Expected {%v, %v}, got {%v, %v}",
			"first", "second",
			dummy.first, dummy.second,
		)
	}
}
