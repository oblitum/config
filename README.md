#oblitum/config

This package is inspired by the infamous post

"Self-referential functions and the design of options"

by Rob Pike.

What I have tried to improve?

- Add full and partial rollbacks
- Support "error handling"
- Generalization (not generics ;\_;) through `interface{}`, type punning, and indirection

Please read the article to get an understanding of what it does, more docs to
come.

Check [the tests](https://github.com/oblitum/config/blob/master/config_test.go)
for usage.
