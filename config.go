package config

type Applier func(configurable interface{}) (Applier, error)

func Configure(configurable interface{}, appliers ...Applier) (rollback Applier, err error) {
	reverters := make([]Applier, 0, len(appliers))

	for _, applier := range appliers {
		if applier != nil {
			var reverter Applier
			if reverter, err = applier(configurable); err != nil {
				break
			}
			reverters = append(reverters, reverter)
		}
	}

	for i, j := 0, len(reverters)-1; i <= j; i, j = i+1, j-1 {
		reverters[i], reverters[j] = reverters[j], reverters[i]
	}

	return func(configurable interface{}) (Applier, error) {
		return Configure(configurable, reverters...)
	}, err
}
